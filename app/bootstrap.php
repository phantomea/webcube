<?php

require __DIR__ . '/../vendor/autoload.php';

$configurator = new Nette\Configurator;

$configurator->setDebugMode(false);
$configurator->setDebugMode((new \Nette\DI\Config\Loader)->load(__DIR__ . '/config/config.neon')['parameters']['lqd_ip']); // load admin IP

//$configurator->setDebugMode('23.75.345.200'); // enable for your remote IP
$configurator->enableTracy(__DIR__ . '/../log');

$configurator->setTimeZone('Europe/Prague');
$configurator->setTempDirectory(__DIR__ . '/../temp');

$configurator->addConfig(__DIR__ . '/config/config.neon');

$configurator->createRobotLoader()
	->addDirectory(__DIR__)
	->register();

if (is_file(__DIR__ . '/config/config.custom.neon')) {
    $configurator->addConfig(__DIR__ . '/config/config.custom.neon');
}
if ($configurator->isDebugMode()) {
    $configurator->addConfig(__DIR__ . '/config/config.debug.neon');
}
if (is_file(__DIR__ . '/config/config.production.neon')) {
    $configurator->addConfig(__DIR__ . '/config/config.production.neon');
} else {
    if (is_file(__DIR__ . '/config/config.local.neon')) {
        $configurator->addConfig(__DIR__ . '/config/config.local.neon');
    }
}

$container = $configurator->createContainer();

return $container;
